;;; harpoon-bookmark.el --- Harpoon? Now you can goon.

;;; Commentary:

;;; You can goon and you can harpoon.  Ok?!

;;; TODO:
;;; Add harpoon-names to harpoon-bookmark-alist-by-file
;;; UI for changing the order of harpoons
;;; Perhaps don't use bookmarks at all?

;;; Code:

(require 'bookmark)
(require 'vc)

(defvar harpoon-bookmark-alist-by-file nil
  "Map of `harpoon-bookmark-alist' for each file.")

;; (defvar-local harpoon-bookmark-alist nil
;;   "Like `bookmark-alist' but not.")

(defvar-local harpoon-names nil
  "List of the names of your precious \\'poons.")

(defvar-local harpoon-default-file nil
  "Like `bookmark-default-file' but not.")

(defcustom harpoon-save-on-change nil
  "If non nil save `harpoon-bookmark-alist' to a file when it is changed."
  :group 'harpoon
  :type 'boolean)

(defcustom harpoon-quick-keys (list ?h ?j ?k ?l ?\;)
  "Keys for `harpoon-jump-quick-keys'."
  :group 'harpoon
  :type '(list integer integer integer integer integer))

(defmacro with-harpoon-bookmark-alist (&rest body)
  "Change the value of `bookmark-alist' for the duration of BODY."
  `(let ((bookmark-alist
          (alist-get
           harpoon-default-file
           harpoon-bookmark-alist-by-file
           nil nil #'string=)))
     ,@body
     (setf (alist-get
            harpoon-default-file
            harpoon-bookmark-alist-by-file
            nil nil #'string=)
           bookmark-alist)))

(defun harpoon--set-default-file ()
  "Generate value for `harpoon-default-file'.
The value will be `vc-root-dir'/harpoons.el if `vc-root-dir' in non
  nil.  Otherwise the value will be `default-directory'/harpoons.el."
  (setq harpoon-default-file (expand-file-name
                              (concat (or
                                       (vc-root-dir)
                                       default-directory)
                                      "harpoons.el"))))

(defun harpoon--set-names ()
  "Set value for `harpoon-names'."
  (with-harpoon-bookmark-alist
   (setq harpoon-names (bookmark-all-names))))

(defun harpoon-save ()
  "Write `harpoon-bookmark-alist' to `harpoon-default-file'."
  (with-harpoon-bookmark-alist
   (bookmark-save nil harpoon-default-file)))

(defun harpoon-load ()
  "Load saved harpoons if a harpoons.el file is found."
  (when (file-exists-p harpoon-default-file)
    (let ((bookmark-alist nil))
      (bookmark-load harpoon-default-file)
      bookmark-alist)))

(defun harpoon--on-exit ()
  "Clear local variables."
  (harpoon-save)
  (setq harpoon-names nil
        harpoon-default-file nil))

(defun harpoon-set ()
  "Set a harpoon using current location."
  (interactive)
  (with-harpoon-bookmark-alist
   (call-interactively #'bookmark-set))
  (harpoon--set-names)
  (when harpoon-save-on-change (harpoon-save)))

(defun harpoon-jump (&optional name)
  "Jump to a harpoon.
If NAME is non nil then jump to harpoon with name NAME, otherwise call prompt
  user to choose a name."
  (interactive)
  (with-harpoon-bookmark-alist
   (if name
       (bookmark-jump name)
     (call-interactively #'bookmark-jump))))

(defun harpoon-jump-quick-keys (char)
  "Quickly jump to a harpoon with a single key.
If CHAR is in `harpoon-quick-keys' and a name exists at the same index in
`harpoon-names' then jump to the harpoon with that name."
  (interactive "cQuick key: ")
  (harpoon--set-names)
  (when-let* ((index (seq-position harpoon-quick-keys char))
              (name (elt harpoon-names index)))
    (harpoon-jump name)))

(defun harpoon-delete ()
  "Delete a harpoon."
  (interactive)
  (with-harpoon-bookmark-alist
   (call-interactively #'bookmark-delete))
  (harpoon--set-names)
  (when harpoon-save-on-change (harpoon-save)))

(defun harpoon-default-bindings ()
  "Set default bindings."
  (global-set-key (kbd "C-c C-h m") #'harpoon-set)
  (global-set-key (kbd "C-c C-h s") #'harpoon-jump)
  (global-set-key (kbd "C-c C-h d") #'harpoon-delete)
  (global-set-key (kbd "C-M-h") #'harpoon-jump-quick-keys))

(defun harpoon-bookmark-mode--turn-on ()
  "Turn on `harpoon-bookmark-mode' in current buffer."
  (harpoon--set-default-file)
  (unless (assoc-string harpoon-default-file harpoon-bookmark-alist-by-file)
    (setf (alist-get harpoon-default-file harpoon-bookmark-alist-by-file) (harpoon-load)))
  (harpoon-default-bindings)
  (harpoon--set-names)
  ;; (add-hook 'kill-emacs-hook #'harpoon--on-exit)
  )

(define-minor-mode harpoon-bookmark-mode
  "Quickly jump to set locations in multiple files."
  :group 'harpoon
  :lighter "Goon Or Poon?"
  (if harpoon-bookmark-mode
      (harpoon-bookmark-mode--turn-on)
    (harpoon--on-exit)))

(define-globalized-minor-mode global-harpoon-bookmark-mode
  harpoon-bookmark-mode
  harpoon-bookmark-mode--turn-on
  :group 'harpoon
  :predicate t)

(provide 'harpoon-bookmark)
;;; harpoon-bookmark.el ends here.
